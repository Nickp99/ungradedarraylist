package arraylist;
 import java.util.ArrayList;
import java.util.List;
/**
 * Hello world!
 *
 */
public class ListPratice 
{
    public static void main( String[] args )
    {
          
          List<String> words = new ArrayList<String>();
          System.out.println("Initial size of words: " + words.size()); 
  
          
          List<String> stillEmpty = new ArrayList<String>(50);
          System.out.println("Initial size of stillEmpty: " + stillEmpty.size()); 
  
         
          String[] strings = {"ABCADAFDSFASDF", "ASDFASDFD342342424", "UPPERCASE", "MixedCase123"};
          for (String s : strings) {
              words.add(s);
          }
  
          
          System.out.println("Size of words after adding strings: " + words.size());
  
          System.out.println(words.contains("ABCADAFDSFASDF")); 
            System.out.println(words.contains("Not")); 
          int count = countUpperCase(words);
          System.out.println("Number of strings with only uppercase letters: " + count);
  
       
          List<String> uppercaseStrings = getUpperCase(words);
          System.out.println("Uppercase strings: " + uppercaseStrings);
    }

    public static int countUpperCase(String[] strings){
        int count=0;

         for (String s : strings) {
            if (isUpperCase(s)) {
                count++;
            }
        }
        return count;
    }
     public static int countUpperCase(List<String> words) {
        int count = 0;

        for (String s : words) {
            if (isUpperCase(s)) {
                count++;
            }
        }

        return count;
    }

    public static List<String> getUpperCase(List<String> words) {
        List<String> uppercaseStrings = new ArrayList<>();

        for (String s : words) {
            if (isUpperCase(s)) {
                uppercaseStrings.add(s);
            }
        }

        return uppercaseStrings;
    }

    public static boolean isUpperCase(String s) {
        return s.matches("[A-Z]+");
    }

    
}
